import { MessageHandler, Profile, RequestProfilesDTO } from '../../interfaces';

export class RequestProfileMessageHandler implements MessageHandler {
    private profiles: Profile[] = [];

    /**
     * Stores the receiving message as the internal state of Handler, used for later calculations.
     * @param message Receiving message from the Edge Server.
     */
    addMessage(message: RequestProfilesDTO) {
        this.profiles.push(...message.profiles);
    }

    /**
     * Calculates average age by gender.
     * @returns Returns average age for females and males.
     */
    public calculateAverageAgeByGender() {
        const females = this.profiles.filter(p => p.gender === 'female');
        const males = this.profiles.filter(p => p.gender === 'male');

        const averageAgeMales = this.calculateAverageAge(males);
        const averageAgeFemales = this.calculateAverageAge(females);

        return {
            averageAgeFemales,
            averageAgeMales,
        }
    }

    /**
     * Calculates average age of the profiles.
     * @param profiles List of profiles.
     * @returns {string | null}
     */
    public calculateAverageAge(profiles: Profile[]): string | null {
        const values = Object.values(profiles);

        if (!values.length) {
            return null;
        }

        const totalSum = values.reduce((previous, current) => {
            return previous + current.age;
        }, 0);

        return (totalSum / values.length).toFixed(2);
    }

    /**
     * Empties the whole dataset, used when new calculations are needed.
     */
    public resetMessagesData() {
        this.profiles = [];
    }
}
