import { Consumers, MessageTypes } from '../../enums';
import { WEBSOCKET_URL } from '../../envVariables';
import ConsumerFactory from '../consumerService/consumerFactory';
import { RequestProfileMessageHandler } from '../messageHandlerService/requestProfileHandler';
import { FileStorage } from '../storageService/fileStorage';

export default class MainService {
    public static start() {
        try {
            const consumer = ConsumerFactory.instantiate(Consumers.WEBSOCKET_CONSUMER, WEBSOCKET_URL);
            consumer.sendMessage(MessageTypes.REQUEST_PROFILES);

            const messageHandler = new RequestProfileMessageHandler();
            consumer.startConsuming(messageHandler);

            setInterval(async () => {
                const results = messageHandler.calculateAverageAgeByGender();
                messageHandler.resetMessagesData();
                // IDEA: If Storage type needs to change on runtime, we can refactor it with Abstract Factory pattern
                const storage = new FileStorage();
                await storage.save({ results });
            }, 60000);
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
    }
}