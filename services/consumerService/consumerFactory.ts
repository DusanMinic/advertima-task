import { Consumers } from '../../enums';
import WebSocketConsumer from './websocketConsumer';

export default class ConsumerFactory {
    public static instantiate(consumer: Consumers, connectionUrl: string) {
        switch (consumer) {
            case Consumers.WEBSOCKET_CONSUMER:
                return new WebSocketConsumer(connectionUrl);
            default:
                throw new Error('Consumer Not Found!');
        }
    }
}
