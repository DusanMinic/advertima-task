import { MessageHandler } from '../../interfaces';

export default abstract class Consumer {
    protected connectionUrl: string;

    constructor(connectionUrl: string) {
        this.connectionUrl = connectionUrl;
    }

    abstract sendMessage(message: string, options: object): void;
    abstract startConsuming(messageHandler: MessageHandler): void;
}