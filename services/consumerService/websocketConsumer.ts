import WebSocket from 'ws';
import { MessageTypes } from '../../enums';
import { MessageHandler, WebSocketMessageDTO } from '../../interfaces';
import Consumer from './consumer';

export default class WebSocketConsumer extends Consumer {
    private ws: WebSocket;

    constructor(websocketUrl: string) {
        super(websocketUrl);
        this.ws = new WebSocket(this.connectionUrl);
    }

    /**
     * Sends the message to the Edge Server.
     * @param messageType Type of message that is sent to the Edge Server.
     * @param param1 Options parameters, not required.
     */
    sendMessage(messageType: MessageTypes, { throttle = 200 } = {}) {
        this.ws.on('open', () => {
            this.ws.send(JSON.stringify({
                type: messageType,
                throttle,
            }));
        })
    }

    /**
     * Starts accepting incoming messages from the server.
     * @param messageHandler Message handler that accepts incoming messages.
     */
    startConsuming(messageHandler: MessageHandler) {
        this.ws.on('message', (data: string) => {
            const message: WebSocketMessageDTO = JSON.parse(data);
            if (message?.data?.data) {
                messageHandler.addMessage(message.data.data);
            }
        });
    }
}


