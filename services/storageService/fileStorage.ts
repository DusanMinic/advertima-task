import { StorageI } from '../../interfaces';
import * as path from 'path';
import { writeFile, mkdir } from 'fs/promises';

export class FileStorage implements StorageI {
    /**
     * Saves the content to File System in the exports directory.
     * @param data Data that needs to be saved.
     * @returns No return value.
     */
    async save(data: object) {
        const folderPath = path.join(__dirname, '../../exports/profiles/averageAge');
        await mkdir(folderPath, { recursive: true });

        const fileName = `averageAgeByGender - ${new Date().toISOString()}.json`;
        const fileContent = JSON.stringify(data, null, 2);
        return await writeFile(`${folderPath}/${fileName}`, fileContent);
    }
}