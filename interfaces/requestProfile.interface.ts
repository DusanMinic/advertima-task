export interface WebSocketMessageDTO {
    type: string;
    data?: MessageStructure;
}

export interface MessageStructure {
    record_type: string;
    data: RequestProfilesDTO;
}

export interface RequestProfilesDTO {
    profiles: Profile[];
    triggerId: string;
    poi: number;
}

export interface Profile {
    age: number;
    gender: string;
    isLookingAtScreen: boolean;
    localTimestamp: number;
    personId: string;
    attentionScore: number;
    coordinates: Coordinates;
}

export interface Coordinates {
    x: number;
    y: number;
    z: number;
}
