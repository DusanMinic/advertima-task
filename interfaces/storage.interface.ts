export interface StorageI {
    save(data: unknown): Promise<void>;
}