# Advertima Consumer Assignment

Advertima Consumer Application is used to connect to Edge Server through Websocket, and request Profile Snapshots as a continuous stream of data.

![diagram](images/diagram.png)

## Installation

In order to start the application, you will need [docker-compose](https://docs.docker.com/compose/install/) installed on your system.



## Usage

To start both the Consumer Client App and Edge Engine Server, type in your terminal:

```bash
docker-compose run --rm advertima-consumer
```

## Tests

To run the Unit and Integration tests, run:
```bash
docker-compose run --rm unit-and-integration-tests
```

Running test example with code coverage:
![tests](images/tests-example.png)

To run the E2E tests, run:
```bash
docker-compose run --rm e2e-tests
```

## Code Design overview
Project was implemented following design best practices (SOLID, YAGNI, KISS, DRY). Structure is flexible, as you can change type of Consumers very easily, without almost any code changes (WS to MQTT for example).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Known issue
Pipeline for E2E tests is failing due to Access denied error while trying to pull the Docker Edge Engine image from the private registry, even though I have `DOCKER_AUTH_CONFIG` ENV variable set for CI/CD. It works locally though.
![ci-error](images/ci-error.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)