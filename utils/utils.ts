/**
 * Simulate an async operation
 * @param seconds Seconds to pass to simulate an async operation
 * @returns {Promise<undefined>} No return value.
 */
export function sleep(seconds: number) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(true);
        }, seconds * 1000);
    });
}