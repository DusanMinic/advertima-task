import { expect } from 'chai';
import Sinon from 'sinon';
import MainService from '../../services/mainService';
import { RequestProfileMessageHandler } from '../../services/messageHandlerService/requestProfileHandler';
import { sleep } from '../../utils/utils';
import { readdir } from 'fs/promises';
import * as path from 'path';
import WebSocketConsumer from '../../services/consumerService/websocketConsumer';

describe('E2E tests for Edge Consumer', () => {
    afterEach(() => {
        Sinon.restore();
    })

    it('Remotely connects to Edge Server and starts receiving profile snapshots', async () => {
        const sendMessageSpy = Sinon.spy(WebSocketConsumer.prototype, 'sendMessage');
        const startConsumingSpy = Sinon.spy(WebSocketConsumer.prototype, 'startConsuming');
        const calculateAverageAgeSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'calculateAverageAgeByGender');
        const resetMessagesDataSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'resetMessagesData');
        const addMessageSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'addMessage');

        const clock = Sinon.useFakeTimers({
            now: new Date(),
            toFake: ['setInterval'],
        });

        MainService.start();

        expect(sendMessageSpy.calledOnce).equals(true);
        expect(startConsumingSpy.calledOnce).equals(true);

        // Simulate time passed until socket receives messages from Edge Server
        await sleep(3);

        expect(addMessageSpy.callCount).to.be.above(0);

        // Advance the timer to the future to trigger setInterval
        clock.tick(65000);

        expect(calculateAverageAgeSpy.calledOnce).equals(true);
        expect(resetMessagesDataSpy.calledOnce).equals(true);

        // Allow File System time to write to disk
        await sleep(1);
        const exportedFiles = await readdir(path.join(__dirname, '../../exports/profiles/'));
        expect(exportedFiles.length).to.be.above(0);
    });
});