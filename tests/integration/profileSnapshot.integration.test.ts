import { expect } from 'chai';
import Sinon from 'sinon';
import WebSocket from 'ws';
import { WebSocketMessageDTO } from '../../interfaces';
import MainService from '../../services/mainService';
import { RequestProfileMessageHandler } from '../../services/messageHandlerService/requestProfileHandler';
import { generateProfiles } from '../unit/messageHandler/helpers/profiles';
import { sleep } from '../../utils/utils';
import WebSocketConsumer from '../../services/consumerService/websocketConsumer';

describe('Profile Snapshot integration tests', () => {
    let websocketServer: WebSocket.Server;
    let websocketMessage: WebSocketMessageDTO;

    before(() => {
        websocketServer = new WebSocket.Server({ port: 3001 });

        websocketMessage = {
            type: 'record',
            data: {
                record_type: 'profiles',
                data: {
                    poi: 1,
                    triggerId: '123',
                    profiles: [
                        ...generateProfiles('male', 2, 25),
                        ...generateProfiles('female', 2, 31),
                    ],
                },
            },
        };

        websocketServer.on('connection', function connection(socket) {
            socket.on('message', function message() {
                socket.send(Buffer.from(JSON.stringify(websocketMessage)));
            });
        });
    });

    after(() => {
        websocketServer.close();
        Sinon.restore();
    });

    it('Connect to websocket, receive profile snapshots, calculateAverageAgeSpy and reset data', async () => {
        const sendMessageSpy = Sinon.spy(WebSocketConsumer.prototype, 'sendMessage');
        const startConsumingSpy = Sinon.spy(WebSocketConsumer.prototype, 'startConsuming');
        const calculateAverageAgeSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'calculateAverageAgeByGender');
        const resetMessagesDataSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'resetMessagesData');
        const addMessageSpy = Sinon.spy(RequestProfileMessageHandler.prototype, 'addMessage');

        const clock = Sinon.useFakeTimers({
            now: new Date(),
            toFake: ['setInterval'],
        });

        MainService.start();

        expect(sendMessageSpy.calledOnce).equals(true);
        expect(startConsumingSpy.calledOnce).equals(true);

        // Simulate time passed until socket receives messages from Edge Server
        await sleep(2);

        expect(addMessageSpy.calledOnce).equals(true);

        // Advance the timer to the future to trigger setInterval
        clock.tick(65000);

        expect(calculateAverageAgeSpy.calledOnce).equals(true);
        expect(resetMessagesDataSpy.calledOnce).equals(true);

    });
});
