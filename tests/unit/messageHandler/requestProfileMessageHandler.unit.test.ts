import { expect } from 'chai';
import { describe, it } from 'mocha';
import { RequestProfileMessageHandler } from '../../../services/messageHandlerService/requestProfileHandler';
import { generateProfiles } from './helpers/profiles';

describe('RequestProfileMessageHandler unit tests', () => {
    describe('addMessage() unit tests', () => {
        const testParameterization = [
            {
                females: 2,
                males: 3,
            },
            {
                females: 15,
                males: 10,
            },
            {
                females: 11,
                males: 19,
            },
            {
                females: 5240,
                males: 1900,
            }
        ];

        testParameterization.forEach(testData => {
            it(`addMessage() method - Adds profile snapshots data with ${testData.females} females and ${testData.males} males`, () => {
                const messageHandler = new RequestProfileMessageHandler();

                const females = generateProfiles('female', testData.females);
                const males = generateProfiles('male', testData.males);

                const requestProfilesDTO = {
                    profiles: [
                        ...females,
                        ...males,
                    ],
                    triggerId: 'c32cac54-bb0e-44bf-8a07-5895feb121a3',
                    poi: 1
                }

                messageHandler.addMessage(requestProfilesDTO);

                const femaleProfiles = messageHandler['profiles'].filter(p => p.gender === 'female');
                const maleProfiles = messageHandler['profiles'].filter(p => p.gender === 'male');

                expect(femaleProfiles.length).to.equals(testData.females);
                expect(maleProfiles.length).to.equals(testData.males);
                expect(messageHandler['profiles'].length).equals(testData.females + testData.males);
            });
        });
    });

    describe('calculateAverageAge() unit tests', () => {
        const testParameterization = [
            {
                profiles: [
                    ...generateProfiles('male', 2, 25),
                    ...generateProfiles('male', 3, 29),
                    ...generateProfiles('male', 4, 17),
                ],
                result: '22.78',
            },
            {
                profiles: [
                    ...generateProfiles('female', 1, 16),
                    ...generateProfiles('female', 1, 21),
                    ...generateProfiles('female', 10, 70),
                ],
                result: '61.42'
            },
        ];

        testParameterization.forEach((testData) => {
            it(`calculateAverageAge on ${testData.profiles.length} profiles`, () => {
                const messageHandler = new RequestProfileMessageHandler();

                const averageAge = messageHandler.calculateAverageAge(testData.profiles);
                expect(averageAge).equals(testData.result);
            });
        });
    });

    describe('calculateAverageAgeByGender() unit tests', () => {
        const testParameterization = [
            {
                females: {
                    profiles: generateProfiles('female', 10, 19),
                    poi: 1,
                    triggerId: '123',
                },
                males: {
                    profiles: generateProfiles('male', 10, 21),
                    poi: 1,
                    triggerId: '1234',
                },
                malesAverageAge: '21.00',
                femalesAverageAge: '19.00',
            },
            {
                females: {
                    profiles: [
                        ...generateProfiles('female', 10, 19),
                        ...generateProfiles('female', 7, 32),
                        ...generateProfiles('female', 3, 41),
                    ],
                    poi: 1,
                    triggerId: '123',
                },
                males: {
                    profiles: [
                        ...generateProfiles('male', 5, 42),
                        ...generateProfiles('male', 3, 78),
                        ...generateProfiles('male', 10, 95),
                    ],
                    poi: 1,
                    triggerId: '1234',
                },
                malesAverageAge: '77.44',
                femalesAverageAge: '26.85',
            },
            {
                females: {
                    profiles: [],
                    poi: 1,
                    triggerId: '123',
                },
                males: {
                    profiles: [],
                    poi: 1,
                    triggerId: '1234',
                },
                malesAverageAge: null,
                femalesAverageAge: null,
            },
        ];

        testParameterization.forEach((testData) => {
            it('calculateAverageAgeByGender() - average age by gender' +
                `with ${testData.females.profiles.length} females and ${testData.males.profiles.length} males`, () => {
                    const messageHandler = new RequestProfileMessageHandler();

                    messageHandler.addMessage(testData.females)
                    messageHandler.addMessage(testData.males)

                    const { averageAgeFemales, averageAgeMales } = messageHandler.calculateAverageAgeByGender();

                    expect(averageAgeFemales).equals(testData.femalesAverageAge);
                    expect(averageAgeMales).equals(testData.malesAverageAge);
                });
        });
    });

    describe('resetProfileData() unit tests', () => {
        it('reset the "profiles" property to empty array when called', () => {
            const messageHandler = new RequestProfileMessageHandler();
            messageHandler.addMessage(
                {
                    poi: 1,
                    triggerId: '321',
                    profiles: generateProfiles('female', 99),
                },
            )

            expect(messageHandler['profiles'].length).equals(99);
            messageHandler.resetMessagesData();
            expect(messageHandler['profiles'].length).equals(0);
        });
    });
});