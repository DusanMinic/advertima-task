import { Profile } from '../../../../interfaces';

/**
 * Generated profiles based on passed parameters
 * @param gender Male or Female profiles to generate.
 * @param numberOfProfiles Number of profiles to generate.
 * @returns Returns generated Profiles
 */
export function generateProfiles(gender: 'female' | 'male', numberOfProfiles: number, age = 23): Profile[] {
    return Array.from({ length: numberOfProfiles }).map((_, index) => ({
        age,
        gender: gender,
        isLookingAtScreen: true,
        localTimestamp: 1615655676111,
        personId: `${index}`,
        attentionScore: 1,
        coordinates: {
            x: 0.41999998688697815,
            y: 8.0600004196167,
            z: 1
        }
    }));
}